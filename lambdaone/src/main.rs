use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use polars::prelude::*;
use serde::{Deserialize, Serialize};
use rusoto_core::{Region};
use rusoto_s3::{GetObjectRequest, S3Client, S3};
use tokio::io::AsyncReadExt;
/// This is a made-up example. Requests come into the runtime as unicode
/// strings in json format, which can map to any structure that implements `serde::Deserialize`
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize)]
struct Request {
    column_name: String,
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into json. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize)]
struct Response {
    req_id: String,
    msg: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let column_name = event.payload.column_name;

    let client = S3Client::new(Region::default());
    let bucket = "lifeexpectancyrustlambda";
    let key = "Life Expectancy Data.csv";

    let request = GetObjectRequest {
        bucket: bucket.to_string(),
        key: key.to_string(),
        ..Default::default()
    };

    let response = client.get_object(request).await?;

// Asynchronously read the body into a buffer
    let body_bytes = response.body.ok_or_else(|| Error::from("S3 object body is missing"))?;
    let mut body_buf = Vec::new();
    body_bytes.into_async_read().read_to_end(&mut body_buf).await?;

    // Create a Cursor to allow re-reading from the buffer
    let body_cursor = std::io::Cursor::new(body_buf);
    

    let max_val = CsvReader::new(body_cursor)
        .infer_schema(Some(10000))
        .has_header(true)
        .finish()?
        .select(&["Country",&column_name])?
        //drop adult mortality null values from the selection
        .drop_nulls(Some(&[&column_name]))?
        .sort(&[&column_name], true,true)?;

        let country = max_val.column("Country").unwrap().get(0).unwrap();
        let mortality = max_val.column(&column_name).unwrap().get(0).unwrap();
    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        msg: format!("The country with the highest {} is {} with a value of {}.", column_name, country, mortality)
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
