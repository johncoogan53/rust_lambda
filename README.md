# Rust Lambda Step Function
Video Link: https://www.youtube.com/watch?v=yYR9ELTP2xA
![alt text](images/stepfunc.png)
This project uses a previously build lambda function which performs data processing, integrates it into a step function workflow to put the output into a dynamodb table. This is a useful framework for defining complex workflows and altering the functionality of pre-built code. This is a compelling example which motivates the developer practice of creating complex things out of the sum of simple parts. This entire workflow could very likely be programed into a single function but what if I wanted to change this to a CLI or change the response endpoint to a web frontend? I would have to restart. With step functions, I can take functional, simple parts and easily chain them together for complex tasks. This can easily be done from the CLI or from the console in the design editor.

One added benefit of this approach. If I programmed all this functionality I would have to be diligent in ensuring I instrumented the function appropriately to manage output/input issues. With step functions, I get that instrumentation for free!

## The Lambda Function

![alt text](images/rust_lambda_architecture.png)

This is a Lambda function which accesses data held within an AWS S3 bucket, processes that data based on the request payload and returns information from that data. Specifically, the S3 bucket has life expectancy data, and the payload options are column names. This lambda function can be invoked with any column name of interest, and it will return a message stating "The country with the highest {column name} is {country} with a value of {column name value for that country}.

This project was developed with a variety of tools. It was developed in gitlabs version of codespaces: Gitpod, which was a convenient cloud development environment very similar to codespaces. This environment comes with AWS tools built in and, as a result, benefited from Amazon Q and Codewhisper autocompletion. I found that both tools were not particularly good at predicting rust syntax although, being a novice, it was likely hard to keep track of what I was attempting to do. 

This lambda function was deployed to AWS by using an IAM user established through the principle of least privilege by only allocating the specific permissions necessary for deployment:
![alt text](images/iam_user_perm.png)
Once deployed, the lambda user was assigned the requisite permissions to access S3:
![alt text](images/lambda_user_perm.png)
Amazon provides a convenient test environment where payloads can be easily generated and run against the deployed lambda, one such payload can be seen below:
![alt text](images/lambda_test.png)
And its result:
![alt text](images/lambda_results.png)

## The Workflow
![alt text](images/builder.png)

Here we can see how the design builder makes the creation of complex workflows easy. Truly as easy as drag and drop. I have now created a data processing tool that pulls from S3, but given it a memory cache. Lets say we wanted to deploy this tool on something like the UN website where you can query specific life expectancy data. Well now, I have added a feature where we can also display past results from the database. This is an example of how easy it can be to add something like this quality of life feature to your front end with step functions.


